namespace Whatsup.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Whatsup.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Whatsup.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "Whatsup.Models.ApplicationDbContext";
            this.AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(Whatsup.Models.ApplicationDbContext context)
        {
            if(context.Offices.FirstOrDefault()==null)
            {
                var office = new Office();
                office.OfficeName = "Avalon Gbg";
                context.Offices.Add(office);

            }

            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                roleManager.Create(new IdentityRole { Name = "Admin" });

            }

            if (!context.Roles.Any(r => r.Name == "Employee"))
            {
                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                roleManager.Create(new IdentityRole { Name = "Employee" });

            }



            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
