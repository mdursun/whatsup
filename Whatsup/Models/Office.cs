﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Whatsup.Models
{
    public class Office
    {
        public Office()
        {
            Missions = new HashSet<Mission>();
        }

        public int Id { get; set; }

        [Required]
        [Display(Name = "Kontor")]
        public string OfficeName { get; set; }

        [Display(Name = "Stad")]
        public string City { get; set; }

        public virtual ICollection<Mission> Missions { get; set; }

    }

}