﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Whatsup.Models
{
    public class Mission
    {
     
        public int Id { get; set; }
        public string Employee { get; set; }

        public string Customer { get; set; }

        public string City { get; set; }

        public DateTime EndDate { get; set; }

        public virtual Office Office {get; set;}

        public bool AtOffice { get; set; }
        
    }
}