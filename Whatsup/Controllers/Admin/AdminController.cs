﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Whatsup.Models;

namespace Whatsup.Controllers.Admin
{
    //[Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        // GET: Admin
        private ApplicationDbContext context = new ApplicationDbContext();
        public ActionResult Index()
        {
            ViewBag.userName = context.Users.Select(o => new SelectListItem { Value = o.UserName, Text = o.UserName });
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);


            ViewBag.Admins = context.Users.ToList().Where(u => userManager.IsInRole(u.Id, "Admin")).ToList();


            return View();
        }



        public ActionResult CreateAdmin(string userName)
        {
            if (!string.IsNullOrEmpty(userName))
            {
                var user = context.Users.Where(u => u.UserName == userName).SingleOrDefault();
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                if (!userManager.IsInRole(user.Id, "Admin"))
                //Adding a role to user.
                {
                    userManager.AddToRole(user.Id, "Admin");
                }

            }
            return RedirectToAction("Index");
        }

        public ActionResult DeleteAdmin(string id)
        {

            var user = context.Users.Where(u => u.Id == id).SingleOrDefault();
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            if (userManager.IsInRole(user.Id, "Admin"))
            {
                userManager.RemoveFromRole(user.Id, "Admin");
            }
            return RedirectToAction("Index");
        }
    }
}