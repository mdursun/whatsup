﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Whatsup.Models;
using Microsoft.AspNet.Identity;


namespace Whatsup.Controllers
{
    public class HomeController : Controller
    {
       
        private ApplicationDbContext context = new ApplicationDbContext();
        public ActionResult Index()
        {
        
            var officeList = context.Offices.ToList();

            ViewBag.officeName = new SelectList(officeList, "OfficeName", "OfficeName");

            return View();
          
        }


        public ActionResult Whatsup(string officeName, string s, string c)
        {

            List<SelectListItem> category = new List<SelectListItem>();

            category.Add(new SelectListItem { Text = "Kund", Value = "Kund" });

            category.Add(new SelectListItem { Text = "Konsult", Value = "Konsult" });

            ViewBag.c = category;

            ViewBag.officeName = officeName;

            if (!string.IsNullOrEmpty(officeName))
            {

                var missions = context.Offices.Where(o => o.OfficeName == officeName).SingleOrDefault().Missions.OrderByDescending(m => m.AtOffice).ToList();

                if (!string.IsNullOrEmpty(s))
                {
                    s = s.ToLower();

                    if (c == "Kund")
                    {
                        missions = missions.Where(m => m.Customer.ToLower().Contains(s)).OrderByDescending(m=> m.AtOffice).ToList();
                    }
                    if (c == "Konsult")
                    {
                        missions = missions.Where(m => m.Employee.ToLower().Contains(s)).OrderByDescending(m => m.AtOffice).ToList();
                    }
                          
                }

                return View(missions);

            }

            else
            {

                return View("_WhatsupEmpty");
            }
            
        }
        [Authorize(Roles = "Employee")]
        public ActionResult CheckInOrOut(int id)
        {
            var mission = context.Missions.Where(m=> m.Id==id).SingleOrDefault();

            var officeName = mission.Office.OfficeName;

            mission.AtOffice = mission.AtOffice ? false : true;

            context.SaveChanges();

            return RedirectToAction("Whatsup", new {officeName = officeName});
        
        }

       
        [Authorize(Roles = "Admin")]
        public ActionResult QuickAdd(string employee, string customer, DateTime enddate, string officeName )
        {
            if (!string.IsNullOrEmpty(employee) && !string.IsNullOrEmpty(customer))
            { 
            var mission = new Mission();
            mission.Office = context.Offices.Where(o => o.OfficeName == officeName).SingleOrDefault();

            mission.AtOffice = false;
            mission.Customer = customer;
            mission.Employee = employee;
            mission.EndDate = enddate;

            context.Missions.Add(mission);

            context.SaveChanges();
            }
            return RedirectToAction("Whatsup", new { officeName = officeName });
          
        }
        [Authorize(Roles = "Employee")]
        public ActionResult QuickEdit(int id, string customer, string city, DateTime enddate) 
        {
           
                var mission = context.Missions.Where(m => m.Id == id).SingleOrDefault();
                var officeName = mission.Office.OfficeName;

                mission.Customer = customer;
                mission.City = city;
                mission.EndDate = enddate;

                context.SaveChanges();

               return RedirectToAction("Whatsup", new { officeName = officeName });
           
        }
       
        [Authorize(Roles = "Admin")]
        public ActionResult QuickDelete(int id)
        {

            var mission=context.Missions.Where(m => m.Id==id).SingleOrDefault();
            var officeName = mission.Office.OfficeName;
            context.Missions.Remove(mission);
            context.SaveChanges();
            return RedirectToAction("Whatsup", new { officeName = officeName });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                context.Dispose();

            base.Dispose(disposing);
        }

      

      
    }
}