﻿$(function () {

    $(".btn-True").val("Ja");
    $(".btn-False").val("Nej");

    $(".btn-quickedit").click(function () {

        var activeTr = $(this).closest("tr");
        //var activediv =$(this).closest("div");
        //activediv.hide();
        activeTr.find(".editable").hide();
        activeTr.find(".quickedit-info").show();

    });

    $(".btn-cancel").click(function () {

        var activeTr = $(this).closest("tr");

        activeTr.find(".editable").show();

        activeTr.find("input:text").val("");

        activeTr.find(".quickedit-info").hide();
      
        $(".quickadd-info").hide();

        $("img").show();


    });

    $(".btn-add").click(function () {

        $(this).hide();
        $(".quickadd-info").show();

    });

});