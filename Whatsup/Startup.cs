﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Whatsup.Startup))]
namespace Whatsup
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
